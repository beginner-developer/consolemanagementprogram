package com.greedy.consol.controller;

import java.util.List;
import java.util.Map;

import com.greedy.consol.model.dto.LightingDTO;
import com.greedy.consol.model.dto.SearchCriteria;
import com.greedy.consol.model.serviece.LightingService;
import com.greedy.consol.views.LightingResultView;

public class LightingController {
	
	private final LightingService lightingService;
	private final LightingResultView lightingResultView;
	
	public LightingController() {
		lightingService = new LightingService();
		lightingResultView = new LightingResultView();
	}
	/* 1. 판매 목록 확인*/
	/* 1-1 판매 목록 전체 조회*/
	public void selectAllList() {
		List<LightingDTO> lightingList = lightingService.selectAllList();
		
		if(lightingList != null && !lightingList.isEmpty()) {
			lightingResultView.LightingResult(lightingList);
		} else {
			lightingResultView.LightingErrorResult("selectList");
		}
		
	}
	/* 1-2 판매 목록 이름 또는 종류(category)로  조회*/
	public void searchList(SearchCriteria searchCriteria) {
		
		List<LightingDTO> lightingList = lightingService.searchList(searchCriteria);
		
		if(lightingList != null && !lightingList.isEmpty()) {
			lightingResultView.LightingResult(lightingList);
		} else {
			lightingResultView.LightingErrorResult("selectList");
		}
		
		
	}
	/* 1-3 판매 금액으로 조회 */
	public void searchListByPrice(int price) {
		List<LightingDTO> lightingList = lightingService.searchListByPrice(price);
		
		if(lightingList != null && !lightingList.isEmpty()) {
			lightingResultView.LightingResult(lightingList);
		} else {
			lightingResultView.LightingErrorResult("selectList");
		}
		
	}
	/* 1-4 상위카테고리로 조회 */
	public void searchListBySubCategory(SearchCriteria searchCriteria) {
			
		List<LightingDTO> lightingList = lightingService.searchListBySubCategory(searchCriteria);
		
		if(lightingList != null && !lightingList.isEmpty()) {
			lightingResultView.LightingResult(lightingList);
		} else {
			lightingResultView.LightingErrorResult("selectList");
		}
	
		
	}
	/* 1-5 제조사 또는 국가로 조회*/
	public void searchListByManufactiurerAndNational(SearchCriteria searchCriteria) {
		
		List<LightingDTO> lightingList = lightingService.searchListByManufactiurerAndNational(searchCriteria);
		
		if(lightingList != null && !lightingList.isEmpty()) {
			lightingResultView.LightingResult(lightingList);
		} else {
			lightingResultView.LightingErrorResult("selectList");
		}
		
	}
	/* 추가할 제품 DTO객체에 값 옯기기*/
	public void registList(Map<String, String> parameter) {
		
		LightingDTO lighting = new LightingDTO();
		
		lighting.setLightingName(parameter.get("name"));
		lighting.setLightingPrice(Integer.parseInt(parameter.get("price")));
		lighting.setCategoryNo(parameter.get("categoryNo"));
		lighting.setBulbYn(parameter.get("bulbYn"));
		lighting.setManufacturerNo(parameter.get("manufacturerNo"));
		lighting.setStockQty(Integer.parseInt(parameter.get("stockQty")));
		
		if(lightingService.insertList(lighting)) {
			lightingResultView.LightingSuccessResult("insert");
		} else {
			lightingResultView.LightingErrorResult("insert");
		}
		
		
	}
	public void modifyList(Map<String, String> parameter) {
		LightingDTO lighting = new LightingDTO();
		
		lighting.setLightingNo(parameter.get("no"));
		lighting.setLightingName(parameter.get("name"));
		if(parameter.get("price").equals("")) {
			lighting.setLightingPrice(0);
		} else {
			lighting.setLightingPrice(Integer.parseInt(parameter.get("price")));
		}
		lighting.setCategoryNo(parameter.get("categoryNo"));
		lighting.setBulbYn(parameter.get("bulbYn"));
		lighting.setManufacturerNo(parameter.get("manufacturerNo"));
		
		if(parameter.get("stockQty").equals("")) {
			lighting.setLightingPrice(0);
		} else {
			lighting.setStockQty(Integer.parseInt(parameter.get("stockQty")));
		}
		
		lighting.setSaleYn(parameter.get("saleYn"));

		
		if(lightingService.updateList(lighting)) {
			lightingResultView.LightingSuccessResult("update");
		} else {
			lightingResultView.LightingErrorResult("update");
		}
		
	}

	public void deleteList(Map<String, String> parameter) {
		LightingDTO lighting = new LightingDTO();
		
		lighting.setLightingNo(parameter.get("lightingNo"));
		
		if(lightingService.deleteList(lighting)) {
			lightingResultView.LightingSuccessResult("delete");
		} else {
			lightingResultView.LightingErrorResult("delete");
		}
		
	}

	

}
