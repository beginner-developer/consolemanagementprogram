package com.greedy.consol.views;

import java.util.List;

import com.greedy.consol.model.dto.LightingDTO;

public class LightingResultView {
	
	public void LightingResult(List<LightingDTO> lightingList) {
		for(LightingDTO lighting : lightingList) {
			System.out.println(lighting);
		}
	}
	
	public void LightingSuccessResult(String successCode) {
		String successMessage = "";
		switch(successCode) {
		case "insert" : successMessage = "판매 목록 등록에 성공하였습니다."; break;
		case "update" : successMessage = "판매 목록 수정에 성공하였습니다."; break;
		case "delete" : successMessage = "판매 목록 삭제에 성공하였습니다."; break;
		}
		System.out.println(successMessage);
	}
	
	public void LightingErrorResult(String errorCode) {
		String errorMessage = "";
		switch(errorCode) {
		case "selectList" : errorMessage = "판매 목록 조회에 실패하였습니다."; break;
		case "selectOne" : errorMessage = "판매 목록 조회에 실패하였습니다."; break;
		case "insert" : errorMessage = "판매 등록에 실패하였습니다."; break;
		case "update" : errorMessage = "판매 수정에 실패하였습니다."; break;
		case "delete" : errorMessage = "판매 삭제에 실패하였습니다."; break;
		}
		System.out.println(errorMessage);
	}
	

}
