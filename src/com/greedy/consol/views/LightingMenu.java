package com.greedy.consol.views;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import com.greedy.consol.controller.LightingController;
import com.greedy.consol.model.dto.SearchCriteria;

public class LightingMenu {
	
	public void mainMenu() {
		
		Scanner sc = new Scanner(System.in);
		LightingController lightingController = new LightingController();
		
		do {
		System.out.println("=======================");
		System.out.println("       빛이 나는 조명       ");
		System.out.println("=======================");
		System.out.println("1. 판매 목록 확인");
		System.out.println("2. 판매 제품 추가");
		System.out.println("3. 판매 제품 수정");
		System.out.println("4. 판매 제품 삭제 (※주의 요망※)");
		System.out.println("9. 종료 하기");
		System.out.print("메뉴 번호 : ");
		int no = sc.nextInt();
		
		switch(no) {
		case 1: selectAllSubmenu(); break;
		case 2: lightingController.registList(inputList()); break;
		case 3: lightingController.modifyList(modifyList());break;
		case 4: lightingController.deleteList(inputLightingNo()); break;
		case 9: return;
		default : {
			System.out.println("잘못된 번호를 입력하였습니다. 다시 확인해 주세요\n");
			} break;
		}
		} while(true);
	}


	/* 판매 조명 조회 메뉴 */
	private static void selectAllSubmenu() {
		Scanner sc = new Scanner(System.in);
		LightingController lightingController = new LightingController();
		
		do {
		System.out.println("=======================");
		System.out.println("       빛이 나는 조명  　    ");
		System.out.println("     　   판매목록  　    ");
		System.out.println("=======================");
		System.out.println("1. 조명 전체 확인하기 ");
		System.out.println("2. 조명 이름 또는 카테고리로 확인하기");
		System.out.println("3. 판매 금액으로 확인하기");
		System.out.println("4. 상위 카테고리로 판매목록 확인하기(메인등, 보조등, 현관등, 전구)");
		System.out.println("5. 제조사나 국가로 확인하기");
		System.out.println("9. 이전 메뉴로 돌아가기");
		System.out.print("메뉴 번호 : ");
		int no = sc.nextInt();
		
		switch(no) {
		case 1: lightingController.selectAllList(); break;
		case 2: lightingController.searchList(inputSearchCriteria()); break;
		case 3: lightingController.searchListByPrice(inputPrice()); break;
		case 4: lightingController.searchListBySubCategory(inputSubCategory()); break;
		case 5: lightingController.searchListByManufactiurerAndNational(inputManufactiurerAndNational());break;
		case 9: return;
		default : {
			System.out.println("잘못된 번호를 입력하였습니다. 다시 확인해 주세요\n");
			} break;
		}
		} while(true);
		
	}
	

	/* 검색조건 name 또는 category 입력 받기*/
	private static SearchCriteria inputSearchCriteria() {
		Scanner sc = new Scanner(System.in);
		do {
		System.out.print("검색 기준을 입력해주세요(name or category) : ");
		String condition = sc.nextLine();
		if(condition.equals("name") || condition.equals("category")) {
			
			System.out.print("검색어를 입력해주세요 : ");
			String value = sc.nextLine();
			
			return new SearchCriteria(condition, value);
			
		} else {
			System.out.println("잘못된 검색 기준을 입력하였습니다. name 또는 category를 입력해주세요");
		}
		} while(true);
		
	}
	/* 검색할 최대 금액 입력 받기 */
	private static int inputPrice() {
		Scanner sc = new Scanner(System.in);
		System.out.println("검색하실 가격의 최대 금액을 입력 해주세요 : ");
		
		return sc.nextInt();
	}

	/*상위 카테고리 입력 받기*/
	private static SearchCriteria inputSubCategory() {
		do {
			Scanner sc = new Scanner(System.in);
			System.out.print("상위 카테고리를 입력해주세요(메인등, 보조등, 현관등, 전구) : ");
			String value = sc.nextLine();
			if (value.equals("메인등") || value.equals("보조등") || value.equals("현관등") || value.equals("전구")) 
			{
				return new SearchCriteria("Category", value);
			} else {
				System.out.println("잘못된 상위 카테고리를 입력하였습니다. 다시 입력해주세요");
			}
		} while (true);

	}
	
	/*제조사 또는 국가 입력 받기*/
	private static SearchCriteria inputManufactiurerAndNational() {
		Scanner sc = new Scanner(System.in);
		do {
		System.out.print("검색 기준을 입력해주세요(manufact or nation) : ");
		String condition = sc.nextLine();
		if(condition.equals("manufact") || condition.equals("nation")) {
			
			System.out.print("검색어를 입력해주세요 : ");
			String value = sc.nextLine();
			
			return new SearchCriteria(condition, value);
			
		} else {
			System.out.println("잘못된 검색 기준을 입력하였습니다. manufact 또는 nation를 입력해주세요");
		}
		} while(true);

	}
	

	/* 추가할 조명 정보 입력 */
	private Map<String, String> inputList() {
		Scanner sc = new Scanner(System.in);
		System.out.print("조명 이름 : ");
		String name = sc.nextLine();
		System.out.print("조명 가격 : ");
		String price = sc.nextLine();
		System.out.print("카테고리 번호 : ");
		String categoryNo = sc.nextLine();
		System.out.print("전구 사용 여부(Y/N) : ");
		String bulbYn = sc.nextLine().toUpperCase();
		System.out.print("제조사 번호 : ");
		String manufacturerNo = sc.nextLine();
		System.out.print("재고 수량 : ");
		String stockQty = sc.nextLine();
		
		Map<String, String> parameter = new HashMap();
		parameter.put("name", name);
		parameter.put("price", price);
		parameter.put("categoryNo", categoryNo);
		parameter.put("bulbYn", bulbYn);
		parameter.put("manufacturerNo", manufacturerNo);
		parameter.put("stockQty", stockQty);
	
		return parameter;
	}
	
	/* 판매 제품 수정할 제품 검색할 번호 및 수정사항 입력 받기*/
	private Map<String, String> modifyList() {
		
		Scanner sc = new Scanner(System.in);
		System.out.println("===== 조명 수정 ===== ");
		System.out.print("검색 할 조명 번호 : ");
		String no = sc.nextLine();
		System.out.print("수정할 조명 이름 : ");
		String name = sc.nextLine();
		System.out.print("수정할 조명 가격 : ");
		String price = sc.nextLine();
		System.out.print("수정할 카테고리 번호 : ");
		String categoryNo = sc.nextLine();
		System.out.print("수정할 전구 사용 여부(Y/N) : ");
		String bulbYn = sc.nextLine().toUpperCase();
		System.out.print("수정할 제조사 번호 : ");
		String manufacturerNo = sc.nextLine();
		System.out.print("수정할 재고 수량 : ");
		String stockQty = sc.nextLine();
		System.out.print("수정할 판매 여부(Y/N) : ");
		String saleYn = sc.nextLine().toUpperCase();
		
		Map<String, String> parameter = new HashMap();
		parameter.put("no", no);
		parameter.put("name", name);
		parameter.put("price", price);
		parameter.put("categoryNo", categoryNo);
		parameter.put("bulbYn", bulbYn);
		parameter.put("manufacturerNo", manufacturerNo);
		parameter.put("stockQty", stockQty);
		parameter.put("saleYn", saleYn);
	
		return parameter;
	}
	
	/* 삭제할 조명 번호 입력 받기 */
	private Map<String, String> inputLightingNo() {
		Scanner sc = new Scanner(System.in);
		System.out.println("===== 판매 목록 삭제 =====");
		System.out.println("※ 한번 삭제하면 복구 할 수 없습니다.※");
		System.out.print("삭제할 조명 번호 입력 : ");
		String lightingNo = sc.nextLine();
		
		Map<String, String> parameter = new HashMap();
		parameter.put("lightingNo", lightingNo);
		return parameter;
	}

}
