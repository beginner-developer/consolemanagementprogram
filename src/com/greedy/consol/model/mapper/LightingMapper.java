package com.greedy.consol.model.mapper;

import java.util.List;
import java.util.Map;

import com.greedy.consol.model.dto.LightingDTO;
import com.greedy.consol.model.dto.SearchCriteria;

public interface LightingMapper {

	/* 판매 목록 전체 조회 */
	List<LightingDTO> selectAllList();

	List<LightingDTO> searchList(SearchCriteria searchCriteria);

	List<LightingDTO> searchListByPrice(Map<String, Integer> map);

	List<LightingDTO> searchListBySubCategory(SearchCriteria searchCriteria);

	List<LightingDTO> searchListByManufactiurerAndNational(SearchCriteria searchCriteria);

	int insertList(LightingDTO lighting);

	int updateList(LightingDTO lighting);

	int deleteList(LightingDTO lighting);

}
