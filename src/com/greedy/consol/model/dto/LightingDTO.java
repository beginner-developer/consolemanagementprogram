package com.greedy.consol.model.dto;

import java.text.SimpleDateFormat;
import java.util.Date;

public class LightingDTO {
	
	private String lightingNo;
	private String lightingName;
	private int lightingPrice;
	private String categoryNo;
	private String bulbYn;
	private String manufacturerNo;
	private int stockQty;
	java.util.Date registDate;
	private String saleYn;
	private String manufacturerName;
	private String nationalName;

	public LightingDTO() {}

	public LightingDTO(String lightingNo, String lightingName, int lightingPrice, String categoryNo, String bulbYn,
			String manufacturerNo, int stockQty, Date registDate, String saleYn, String manufacturerName,
			String nationalName) {
		super();
		this.lightingNo = lightingNo;
		this.lightingName = lightingName;
		this.lightingPrice = lightingPrice;
		this.categoryNo = categoryNo;
		this.bulbYn = bulbYn;
		this.manufacturerNo = manufacturerNo;
		this.stockQty = stockQty;
		this.registDate = registDate;
		this.saleYn = saleYn;
		this.manufacturerName = manufacturerName;
		this.nationalName = nationalName;
	}

	public String getLightingNo() {
		return lightingNo;
	}

	public void setLightingNo(String lightingNo) {
		this.lightingNo = lightingNo;
	}

	public String getLightingName() {
		return lightingName;
	}

	public void setLightingName(String lightingName) {
		this.lightingName = lightingName;
	}

	public int getLightingPrice() {
		return lightingPrice;
	}

	public void setLightingPrice(int lightingPrice) {
		this.lightingPrice = lightingPrice;
	}

	public String getCategoryNo() {
		return categoryNo;
	}

	public void setCategoryNo(String categoryNo) {
		this.categoryNo = categoryNo;
	}

	public String getBulbYn() {
		return bulbYn;
	}

	public void setBulbYn(String bulbYn) {
		this.bulbYn = bulbYn;
	}

	public String getManufacturerNo() {
		return manufacturerNo;
	}

	public void setManufacturerNo(String manufacturerNo) {
		this.manufacturerNo = manufacturerNo;
	}

	public int getStockQty() {
		return stockQty;
	}

	public void setStockQty(int stockQty) {
		this.stockQty = stockQty;
	}

	public java.util.Date getRegistDate() {
		return registDate;
	}

	public void setRegistDate(java.util.Date registDate) {
		this.registDate = registDate;
	}

	public String getSaleYn() {
		return saleYn;
	}

	public void setSaleYn(String saleYn) {
		this.saleYn = saleYn;
	}

	public String getManufacturerName() {
		return manufacturerName;
	}

	public void setManufacturerName(String manufacturerName) {
		this.manufacturerName = manufacturerName;
	}

	public String getNationalName() {
		return nationalName;
	}

	public void setNationalName(String nationalName) {
		this.nationalName = nationalName;
	}

	@Override
	public String toString() {
		
		SimpleDateFormat registDateFormat = new SimpleDateFormat("yyyy년 MM월 dd일 E요일 등록");
		String rDate = registDateFormat.format(registDate);
		
		String temp = null;
		String nation = "";
		String manufact = "";
		
		if(nationalName!=null){
			temp = ", 제조 국가명 : ";
			nation = temp + nationalName; 
		}
		if(manufacturerName!=null){
			temp = ", 제조사명 : ";
			manufact = temp + manufacturerName; 
		} 
		
		return 
	
		"조명 번호 : " + lightingNo +", 조명 이름 : " + lightingName + ", 금액 : " + lightingPrice + "원, 카테고리 번호 : " + categoryNo
		 + ", 전구 사용여부 : " + bulbYn + ", 제조사 번호 : " + manufacturerNo + ", 재고 수량 : "+ stockQty + "개, 등록일자 : " + rDate
		 +", 판매여부 : " +  saleYn + manufact + nation;
	
	}

	
		
}
