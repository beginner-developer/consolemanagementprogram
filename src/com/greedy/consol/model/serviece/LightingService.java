package com.greedy.consol.model.serviece;

import static com.greedy.consol.common.Template.getSqlSession;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;

import com.greedy.consol.model.dto.LightingDTO;
import com.greedy.consol.model.dto.SearchCriteria;
import com.greedy.consol.model.mapper.LightingMapper;

public class LightingService {
	
	private LightingMapper lightingMapper;

	public List<LightingDTO> selectAllList() {
		
		SqlSession sqlSession = getSqlSession();
		
		lightingMapper = sqlSession.getMapper(LightingMapper.class);
		List<LightingDTO> lightingList = lightingMapper.selectAllList();
		
		sqlSession.close();
		
		return lightingList;
	}

	public List<LightingDTO> searchList(SearchCriteria searchCriteria) {
		
		SqlSession sqlSession = getSqlSession();
		
		lightingMapper = sqlSession.getMapper(LightingMapper.class);
		List<LightingDTO> lightingList = lightingMapper.searchList(searchCriteria);
		
		sqlSession.close();
		
		return lightingList;
	}

	public List<LightingDTO> searchListBySubCategory(SearchCriteria searchCriteria) {
		
		SqlSession sqlSession = getSqlSession();
		
		lightingMapper = sqlSession.getMapper(LightingMapper.class);
		List<LightingDTO> lightingList = lightingMapper.searchListBySubCategory(searchCriteria);
		
		sqlSession.close();
		
		return lightingList;
	}

	public List<LightingDTO> searchListByPrice(int price) {
		
		SqlSession sqlSession = getSqlSession();
		
		lightingMapper = sqlSession.getMapper(LightingMapper.class);
		
		Map<String, Integer> map = new HashMap<>();
		map.put("price", price);
		List<LightingDTO> lightingList = lightingMapper.searchListByPrice(map);
		
		sqlSession.close();
		
		return lightingList;
	}

	public List<LightingDTO> searchListByManufactiurerAndNational(SearchCriteria searchCriteria) {
		
		SqlSession sqlSession = getSqlSession();
		
		lightingMapper = sqlSession.getMapper(LightingMapper.class);
		List<LightingDTO> lightingList = lightingMapper.searchListByManufactiurerAndNational(searchCriteria);
		
		sqlSession.close();
		
		return lightingList;
	}

	public boolean insertList(LightingDTO lighting) {
		
		SqlSession sqlSession = getSqlSession();
		lightingMapper = sqlSession.getMapper(LightingMapper.class);
		
		int result = lightingMapper.insertList(lighting);
		
		if(result > 0) {
			sqlSession.commit();
		} else {
			sqlSession.rollback();
		}
		
		sqlSession.close();
		
		return result > 0 ? true : false;
	}

	public boolean updateList(LightingDTO lighting) {
		
		SqlSession sqlSession = getSqlSession();
		lightingMapper = sqlSession.getMapper(LightingMapper.class);
		
		int result = lightingMapper.updateList(lighting);
		
		if(result > 0) {
			sqlSession.commit();
		} else {
			sqlSession.rollback();
		}
		
		sqlSession.close();
		
		return result > 0 ? true : false;
	}

	public boolean deleteList(LightingDTO lighting) {
		
		SqlSession sqlSession = getSqlSession();
		lightingMapper = sqlSession.getMapper(LightingMapper.class);
		
		int result = lightingMapper.deleteList(lighting);
		
		if(result > 0) {
			sqlSession.commit();
		} else {
			sqlSession.rollback();
		}
		
		sqlSession.close();
		
		return result > 0 ? true : false;
	}

}
